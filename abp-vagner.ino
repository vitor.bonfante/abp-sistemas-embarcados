#define LATCH_DIO 4
#define CLK_DIO 7
#define DATA_DIO 8
#define H 0
#define L 1
#define I INPUT
#define O OUTPUT

volatile byte seconds = 00;
volatile byte minutes = 00;
volatile byte hours = 00;

unsigned long mil = millis();

const byte Numbers[] = {0xC0, 0xF9, 0xA4, 0xB0, 0x99, 0x92, 0x82, 0xF8, 0X80, 0X90};
const byte Display[] = {0xF1, 0xF2, 0xF4, 0xF8};

const int btn1 = A1, btn2 = A2, btn3 = A3, led1 = 13, led2 = 12, led3 = 11, led4 = 10, buzzerPin = 3;

int alarmHour = 0;
int alarmMin = 20;
int editingPin = 3;
int potRead;

bool isAlarmEnabled = false;
bool isEditing = false;
bool playBuzzer = false;

volatile uint8_t lastClick1;
volatile uint8_t lastClick2;
volatile uint8_t lastClick3;

enum clockModes
{
  mode_clock_time,
  mode_clock_set_time,
  mode_clock_set_alarm
};

byte mode = mode_clock_set_time;

void setup()
{
  myPinMode(btn1, I);
  myPinMode(btn2, I);
  myPinMode(btn3, I);

  myPinMode(led1, O);
  myPinMode(led2, O);
  myPinMode(led3, O);
  myPinMode(led4, O);

  myPinMode(buzzerPin, O);

  myPinMode(LATCH_DIO, O);
  myPinMode(CLK_DIO, O);
  myPinMode(DATA_DIO, O);

  myDigitalWrite(led1, L);
  myDigitalWrite(led2, L);
  myDigitalWrite(led3, L);
  myDigitalWrite(led4, L);

  myDigitalWrite(buzzerPin, L);

  Serial.begin(9600);
}

void loop()
{
  ADMUX |= 0b01000000;
  ADCSRA |= 0b11000000;

  potRead = ADCL | (ADCH << 8);

  bool btnRead1 = myReadFunction(btn1);
  bool btnRead2 = myReadFunction(btn2);
  bool btnRead3 = myReadFunction(btn3);

  switch (mode)
  {
  case mode_clock_time:
    timeCount();
    break;
  case mode_clock_set_time:
    setTime();
    break;
  case mode_clock_set_alarm:
    setAlarm();
    break;
  }
  // entra no modo edição tempo
  if (btnRead1 == H && !isEditing && lastClick1 != btnRead1)
  {
    mode = mode_clock_set_time;
  }

  // entra no modo edição alarme
  if (btnRead2 == H && !isEditing && lastClick2 != btnRead2)
  {
    mode = mode_clock_set_alarm;
  }

  // liga/desliga alarme e mostra
  if (btnRead3 == H && !isEditing && lastClick3 != btnRead3)
  {
    isAlarmEnabled = !isAlarmEnabled;
  }

  lastClick1 = btnRead1;
  lastClick2 = btnRead2;
  lastClick3 = btnRead3;

  isAlarmEnabled ? myDigitalWrite(led4, H) : myDigitalWrite(led4, L);
  if (playBuzzer)
  {
    beepBeep();
  }
}

void beepBeep()
{
  tone(buzzerPin, potRead * 5);
  PCICR |= (1 << PCIE1);
  PCMSK1 |= (1 << PCINT11);
}

ISR(PCINT1_vect)
{
  if (playBuzzer)
  {
    noTone(buzzerPin);
    myDigitalWrite(buzzerPin, H);
    playBuzzer = false;
    isAlarmEnabled = true;
  }
}

void editingLeds(int digit)
{
  switch (digit)
  {
  case 0:
    myDigitalWrite(led1, H);
    myDigitalWrite(led2, L);
    myDigitalWrite(led3, L);
    myDigitalWrite(led4, L);
    break;
  case 1:
    myDigitalWrite(led1, L);
    myDigitalWrite(led2, H);
    myDigitalWrite(led3, L);
    myDigitalWrite(led4, L);
    break;
  case 2:
    myDigitalWrite(led1, L);
    myDigitalWrite(led2, L);
    myDigitalWrite(led3, H);
    myDigitalWrite(led4, L);
    break;
  case 3:
    myDigitalWrite(led1, L);
    myDigitalWrite(led2, L);
    myDigitalWrite(led3, L);
    myDigitalWrite(led4, H);
    break;
  }
}
// -------horario--------
void upTime(int digit)
{
  int backupHours = hours;
  int backupMinutes = minutes;

  switch (digit)
  {
  case 0:
    if ((hours / 10 == 2) || (hours / 10 == 1 && hours % 10 > 3))
    {
      hours = -10 + backupHours % 10;
    }
    hours += 10;
    break;
  case 1:
    if (hours % 10 == 9)
    {
      hours -= 10;
    }
    if (hours / 10 == 2 && hours % 10 == 3)
    {
      hours -= 4;
    }
    hours += 1;
    break;
  case 2:
    if (minutes / 10 == 5)
    {
      minutes = -10 + backupMinutes % 10;
    }
    minutes += 10;
    break;
  case 3:
    if (minutes % 10 == 9)
    {
      minutes -= 10;
    }
    minutes += 1;
    break;
  }
}

void downTime(int digit)
{
  int backupHours = hours;
  int backupMinutes = minutes;

  switch (digit)
  {
  case 0:
    if (hours / 10 == 0)
    {
      hours = +10 + backupHours % 10;
    }
    hours -= 10;
    break;
  case 1:
    if (hours % 10 == 0)
    {
      hours += 1;
    }
    hours -= 1;
    break;
  case 2:
    if (minutes / 10 == 0)
    {
      minutes = +10 + backupMinutes % 10;
    }
    minutes -= 10;
    break;
  case 3:
    if (minutes % 10 == 0)
    {
      minutes += 1;
    }
    minutes -= 1;
    break;
  }
}

void setTime()
{
  isEditing = true;
  bool btnRead1 = myReadFunction(btn1);
  bool btnRead2 = myReadFunction(btn2);
  bool btnRead3 = myReadFunction(btn3);

  if (btnRead1 == H && lastClick1 != btnRead1)
  {
    // sobe tempo
    upTime(editingPin);
  }
  if (btnRead2 == H && lastClick2 != btnRead2)
  {
    // diminui tempo
    downTime(editingPin);
  }

  if (btnRead3 == H && lastClick3 != btnRead3)
  {
    editingPin--;
    if (editingPin < 0)
    {
      editingPin = 3;
      mode = mode_clock_time;
      isEditing = false;
    }
  }

  lastClick1 = btnRead1;
  lastClick2 = btnRead2;
  lastClick3 = btnRead3;

  displayWrite(0, hours / 10);
  displayWrite(1, hours % 10);
  displayWrite(2, minutes / 10);
  displayWrite(3, minutes % 10);
  editingLeds(editingPin);
}
// ---------------------

// ------alarme------
void upTimeAlarm(int digit)
{
  int backupAlarmHour = alarmHour;
  int backupAlarmMin = alarmMin;

  switch (digit)
  {
  case 0:
    if ((alarmHour / 10 == 2) || (alarmHour / 10 == 1 && alarmHour % 10 > 3))
    {
      alarmHour = -10 + backupAlarmHour % 10;
    }
    alarmHour += 10;
    break;
  case 1:
    if (alarmHour % 10 == 9)
    {
      alarmHour -= 10;
    }
    if (alarmHour / 10 == 2 && alarmHour % 10 == 3)
    {
      alarmHour -= 4;
    }
    alarmHour += 1;
    break;
  case 2:
    if (alarmMin / 10 == 5)
    {
      alarmMin = -10 + backupAlarmMin % 10;
    }
    alarmMin += 10;
    break;
  case 3:
    if (alarmMin % 10 == 9)
    {
      alarmMin -= 10;
    }
    alarmMin += 1;
    break;
  }
}

void downTimeAlarm(int digit)
{
  int backupAlarmHour = alarmHour;
  int backupAlarmMin = alarmMin;

  switch (digit)
  {
  case 0:
    if (alarmHour / 10 == 0)
    {
      alarmHour = +10 + backupAlarmHour % 10;
    }
    alarmHour -= 10;
    break;
  case 1:
    if (alarmHour % 10 == 0)
    {
      alarmHour += 1;
    }
    alarmHour -= 1;
    break;
  case 2:
    if (alarmMin / 10 == 0)
    {
      alarmMin = +10 + backupAlarmMin % 10;
    }
    alarmMin -= 10;
    break;
  case 3:
    if (alarmMin % 10 == 0)
    {
      alarmMin += 1;
    }
    alarmMin -= 1;
    break;
  }
}

void setAlarm()
{
  isEditing = true;
  bool btnRead1 = myReadFunction(btn1);
  bool btnRead2 = myReadFunction(btn2);
  bool btnRead3 = myReadFunction(btn3);

  if (btnRead1 == H && lastClick1 != btnRead1)
  {
    // sobe tempo
    upTimeAlarm(editingPin);
  }
  if (btnRead2 == H && lastClick2 != btnRead2)
  {
    // diminui tempo
    downTimeAlarm(editingPin);
  }

  if (btnRead3 == H && lastClick3 != btnRead3)
  {
    editingPin--;
    if (editingPin < 0)
    {
      editingPin = 3;
      mode = mode_clock_time;
      isEditing = false;
    }
  }

  lastClick1 = btnRead1;
  lastClick2 = btnRead2;
  lastClick3 = btnRead3;

  displayWrite(0, alarmHour / 10);
  displayWrite(1, alarmHour % 10);
  displayWrite(2, alarmMin / 10);
  displayWrite(3, alarmMin % 10);
  editingLeds(editingPin);
}
// -------------------
void checkTime()
{
  if ((alarmHour == hours) && (alarmMin == minutes) && isAlarmEnabled)
  {
    playBuzzer = true;
  }
  else
  {
    myDigitalWrite(buzzerPin, L);
  }
}

void myPinMode(int pin, int mode)
{
  if (pin >= 0 && pin <= 7)
  {
    if (mode == INPUT)
    {
      DDRD &= ~(1 << pin);
    }
    else
    {
      DDRD |= (1 << pin);
    }
  }
  else if (pin >= 8 && pin <= 13)
  {
    if (mode == INPUT)
    {
      DDRB &= ~(1 << pin - 8);
    }
    else
    {
      DDRB |= (1 << pin - 8);
    }
  }
  else if (pin >= 14 && pin <= 19)
  {
    if (mode == INPUT)
    {
      DDRC &= ~(1 << pin - 14);
    }
    else
    {
      DDRC |= (1 << pin - 14);
    }
  }
}

void myDigitalWrite(int pin, int value)
{
  if (pin >= 0 && pin <= 7)
  {
    if (value == 0)
    {
      PORTD &= ~(1 << pin);
    }
    else
    {
      PORTD |= (1 << pin);
    }
  }
  else if (pin >= 8 && pin <= 13)
  {
    if (value == 0)
    {
      PORTB &= ~(1 << pin - 8);
    }
    else
    {
      PORTB |= (1 << pin - 8);
    }
  }
  else if (pin >= 14 && pin <= 19)
  {
    if (value == 0)
    {
      PORTC &= ~(1 << pin - 14);
    }
    else
    {
      PORTC |= (1 << pin - 14);
    }
  }
}

bool myReadFunction(int pin)
{
  bool r;
  if (pin >= 0 && pin <= 7)
  {
    r = (PIND & (1 << pin));
  }
  else if (pin >= 8 && pin <= 13)
  {
    r = (PINB & (1 << (pin - 8)));
  }
  else if (pin >= 14 && pin <= 19)
  {
    r = (PINC & (1 << (pin - 14)));
  }
  return r;
}

void displayWrite(byte segment, byte value)
{
  myDigitalWrite(LATCH_DIO, L);
  shiftOut(DATA_DIO, CLK_DIO, MSBFIRST, Numbers[value]);
  shiftOut(DATA_DIO, CLK_DIO, MSBFIRST, Display[segment]);
  myDigitalWrite(LATCH_DIO, H);
}

void runTime()
{
  seconds++;
  if (seconds > 59)
  {
    seconds = 0;
    minutes++;
    if (minutes > 59)
    {
      minutes = 0;
      hours++;
      if (hours > 23)
      {
        hours = 0;
      }
    }
  }
}

void timeCount()
{
  if ((mil - millis()) < 1)
  {
    runTime();
    checkTime();
  }
  if ((millis() - mil) > 1 * 2)
  {
    mil = millis();
    runTime();
    checkTime();
  }

  displayWrite(0, hours / 10);
  displayWrite(1, hours % 10);
  displayWrite(2, minutes / 10);
  displayWrite(3, minutes % 10);
}